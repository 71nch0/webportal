﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RestAPIDemo.API.Services;
using RestAPIDemo.API.ViewModels;
using RestAPIDemo.Database.Models;
using System.Threading.Tasks;

namespace RestAPIDemo.Controllers
{
    [Controller]
    [Route("[controller]")]
    public class PrioritiesController : Controller
    {
        private readonly IMapper mapper;
        private readonly IModelService<Priority> service;

        public PrioritiesController(IModelService<Priority> service, IMapper mapper)
        {
            this.mapper = mapper;
            this.service = service;
        }

        [HttpPost("create")]
        public IActionResult AddItem([FromBody] PriorityViewModel priority)
        {
            Priority mappedPriority = mapper.Map<Priority>(priority);
            var result = service.Add(mappedPriority);
            return Ok(result);
        }

        [HttpPost("createAsync")]
        public async Task<IActionResult> AddriorityAsync([FromBody] PriorityViewModel priority)
        {
            Priority mappedPriority = mapper.Map<Priority>(priority);
            var result = await service.AddAsync(mappedPriority);
            return Ok(result);
        }

        [HttpGet]
        public string Get()
        {
            return "Helloooo";
        }
    }
}