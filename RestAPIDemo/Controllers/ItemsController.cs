﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RestAPIDemo.API.Services;
using RestAPIDemo.API.ViewModels;
using RestAPIDemo.Database.Models;
using System.Threading.Tasks;

namespace RestAPIDemo.Controllers
{
    [Controller]
    [Route("[controller]")]
    public class ItemsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IModelService<Item> service;

        public ItemsController(IModelService<Item> service, IMapper mapper)
        {
            this.mapper = mapper;
            this.service = service;
        }

        [HttpPost("create")]
        public IActionResult AddItem([FromBody] ItemViewModel item)
        {            
            Item mappedItem = mapper.Map<Item>(item);
            var result = service.Add(mappedItem);
            return Ok(result);
        }

        [HttpPost("createAsync")]
        public async Task<IActionResult> AddItemAsync([FromBody] Item item)
        {
            Item mappedItem = mapper.Map<Item>(item);
            var result = await service.AddAsync(mappedItem);
            return Ok(result);            
        }

        [HttpGet]
        public string Get()
        {
            return "Helloooo";
        }
    }
}