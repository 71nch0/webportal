﻿using RestAPIDemo.Database.Models;
using RestAPIDemo.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.API.Services
{
    public class ModelService<T> : IModelService<T> where T : class
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepository<T> repository;

        public ModelService(IUnitOfWork unitOfWork, IRepository<T> repository)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        public T Add(T obj)
        {
            try
            {
                repository.Add(obj);
            }
            catch (Exception e)
            {                
                throw e;
            }
            unitOfWork.Commit();
            return obj;
        }

        public async Task<T> AddAsync(T obj)
        {
            try
            {
                await repository.AddAsync(obj);
            }
            catch (Exception e)
            {
                throw e;
            }
            await unitOfWork.CommitAsync();
            return obj;
        }
    }
}
