﻿using System.Threading.Tasks;

namespace RestAPIDemo.API.Services
{
    public interface IModelService<T> where T: class
    {
        T Add(T obj);
        Task<T> AddAsync(T obj);
    }
}
