﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestAPIDemo.API.Services;
using RestAPIDemo.Database;
using RestAPIDemo.Database.Repositories;
using AutoMapper;
using RestAPIDemo.API.ViewModels;

namespace RestAPIDemo
{
    public class Startup
    {
        private IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {   
            services.AddAutoMapper(typeof(Startup));
            services.AddSingleton(Configuration);
            services.AddScoped<DbContext, WebPortalDBContext>();
            services.AddDbContext<WebPortalDBContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("WebPortalDB"))
            );
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IModelService<>), typeof(ModelService<>));
            services.AddCors();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {            
            app.UseCors();
            app.UseMvc();
        }
    }
}
