﻿using AutoMapper;
using RestAPIDemo.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPIDemo.API.ViewModels
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Item, ItemViewModel>().ReverseMap();
            CreateMap<Priority, PriorityViewModel>().ReverseMap();
        }
    }
}
