﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RestAPIDemo.Database.Models;

namespace RestAPIDemo.Database.Configurations
{
    public class ItemConfiguration: IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.HasKey(v => v.Id);
            builder.Property(v => v.Id).UseSqlServerIdentityColumn();
            builder.Property(v => v.Title);
            builder.Property(v => v.Description);
            builder.Property(v => v.LevelId);
        }        
    }
}
