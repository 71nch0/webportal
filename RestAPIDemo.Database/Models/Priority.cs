﻿using System.Collections.Generic;

namespace RestAPIDemo.Database.Models
{
    public class Priority
    {        
        public int Id { get; set; }        
        public string PriorityName { get; set; }
        public ICollection<Item> Items { get; set; }
    }
}
