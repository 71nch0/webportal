﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RestAPIDemo.Database.Models
{
    public class Item
    {        
        public int Id { get; set; }        
        public string Title { get; set; }
        public string Description { get; set; }
        [ForeignKey("Level")]
        public int LevelId { get; set; }        
        public Priority Level { get; set; }
    }
}
