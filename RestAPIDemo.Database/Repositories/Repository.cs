﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RestAPIDemo.Database.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IUnitOfWork unitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public T Add(T entity)
        {
            unitOfWork.Context.Set<T>().Add(entity);
            return entity;
        }

        public Task AddAsync(T entity)
        {
            return unitOfWork.Context.Set<T>().AddAsync(entity);
        }

        public void Delete(T entity)
        {
            unitOfWork.Context.Set<T>().Remove(entity);
        }

        public T Find(Expression<Func<T, bool>> match)
        {
            return unitOfWork.Context.Set<T>().Find(match);
        }
        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await unitOfWork.Context.Set<T>().SingleOrDefaultAsync(match);
        }

        public ICollection<T> FindAll(Expression<Func<T, bool>> match)
        {
            return unitOfWork.Context.Set<T>().Where(match).ToList();
        }

        public async Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await unitOfWork.Context.Set<T>().Where(match).ToListAsync();
        }

        public T FindById(int id)
        {
            return unitOfWork.Context.Set<T>().Find(id);
        }

        public async Task<T> FindByIdAsync(int id)
        {
            return await unitOfWork.Context.Set<T>().FindAsync(id);
        }

        public T Update(T entity)
        {
            T entityToUpdate = unitOfWork.Context.Set<T>().Find(entity);
            if (entityToUpdate != null)
            {
                unitOfWork.Context.Entry(entityToUpdate).CurrentValues.SetValues(entity);
            }
            return entityToUpdate;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            T entityToUpdate = await unitOfWork.Context.Set<T>().FindAsync(entity);
            if (entityToUpdate != null)
            {
                unitOfWork.Context.Entry(entityToUpdate).CurrentValues.SetValues(entity);
            }
            return entityToUpdate;
        }
    }
}
