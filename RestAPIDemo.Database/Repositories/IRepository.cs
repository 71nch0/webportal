﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RestAPIDemo.Database.Repositories
{
    public interface IRepository<T> where T : class
    {
        T Add(T entity);
        Task AddAsync(T entity);
        void Delete(T entity);
        T Find(Expression<Func<T, bool>> match);
        Task<T> FindAsync(Expression<Func<T, bool>> match);
        ICollection<T> FindAll(Expression<Func<T, bool>> match);
        Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match);
        T FindById(int id);
        Task<T> FindByIdAsync(int id);
        T Update(T entity);
        Task<T> UpdateAsync(T entity);
    }
}
